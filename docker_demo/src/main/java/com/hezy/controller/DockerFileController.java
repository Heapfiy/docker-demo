package com.hezy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/docker")
public class DockerFileController {

    @GetMapping
    public String getDockerFile() {
        return "Hello Dockerfile!";
    }
}
